import { EVENTS_NAME } from '../consts';
import { Button } from '../classes/button';

export class OverlayScene extends Phaser.Scene {
  private resizeHandler!: () => void;
  private playBtn?: Button;
  private logo?: Phaser.GameObjects.Sprite;

  constructor() {
    super('overlay-scene');

    this.resizeHandler = () => {
      this.updateChildrensPosition();
    };
  }

  create(): void {
    this.initPlayBtn();
    this.initLogo();
    this.updateChildrensPosition();
    this.game.scale.addListener(EVENTS_NAME.resize, this.resizeHandler);
  }

  private initPlayBtn(): void {
    const btnConfig = {
      scene: this,
      x: this.game.scale.width - 16,
      y: this.game.scale.height - 16,
      frame: 'btn-play-now',
      isPulsed: true,
    };

    this.playBtn = new Button(btnConfig).setOrigin(1, 1);
    this.playBtn.setInteractive();

    this.playBtn.on('pointerdown', this.redirect);
  }

  private initLogo(): void {
    this.logo = new Phaser.GameObjects.Sprite(this, 16, 16, 'spritesheet', 'logo');
    this.add.existing(this.logo);
  }

  private redirect(): void {
    window.location.href = 'http://play.google.com/store/apps/';
  }

  private updateChildrensPosition(): void {
    const centerX = this.game.scale.width / 2;
    const centerY = this.game.scale.height / 2;

    if (this.logo) {
      this.game.scale.isLandscape
        ? this.logo.setOrigin(0, 0).setPosition(16, 16)
        : this.logo.setOrigin(0.5, 0).setPosition(centerX, 16);
    }

    if (this.playBtn)
      this.playBtn.setPosition(this.game.scale.width - 16, this.game.scale.height - 16);
  }
}
